﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class CuerpoTecnico:Persona
    {
        public bool Principal { get; set; }

        public int NumeroAyudante { get; set; }

        public override bool ProximoRetiro()
        {
            return false;
        }
    }
}
