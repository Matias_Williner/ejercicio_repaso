﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Persona
    {
        public string Nombre { get; set; }
        public int Edad { get; set; }
        public string Nacionalidad { get; set; }

        ////////////////////////////////////////////// 4 ////////////////////////////////////////////////
        public List<string> ObtenerNombreNacionalidad ()
        {
            List<string> listaNombreNacionalidad = new List<string>();
            listaNombreNacionalidad.Add(Nombre);
            listaNombreNacionalidad.Add(Nacionalidad);
            return listaNombreNacionalidad;
        }

        ////////////////////////////////////////////// 5 ////////////////////////////////////////////////
        public virtual bool ProximoRetiro()
        {
            if (Edad>=36)
            {
                return true;
            }
            return false;
        }
    }
}
