﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Jugador:Persona
    {
        public int Numero { get; set; }
        public string Estado { get; set; }

        public Posiciones Posicion { get;set; }

        public Equipo Equipo { get; set; }

        public override bool ProximoRetiro()
        {
            return base.ProximoRetiro();
        }
        public enum Posiciones
        {
            Arquero=1,
            Defensor=2,
            Mediocampista=3, 
            Atacante=4,
        }

    }
}
