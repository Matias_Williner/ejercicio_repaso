﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Partido
    {
        public List<Arbitro> Arbitros { get; set; }

        public string Ciudad { set; get; }

        public DateTime FechaHoraInicio {get;set;}

        public Equipo Equipo1 { get; set; }

        public Equipo Equipo2 { get; set; }

        public List<Gol> GolesEquipo1 { get; set; }

        public List<Gol> GolesEquipo2 { get; set; }

        public int TiempoJuego { get; set; }

        public List<Cambio> Cambios { get; set; }

        public List<Tarjeta> TarjetasAmarillas { get; set; }

        public List<Tarjeta> TarjetasRojas { get; set; }

        //////////////////////////////////////////////////////// 7 //////////////////////////////////////////////////////
        public bool CargarIncidencia(Jugador jugador, bool enContra, int minutoJuego, string arco)
        {
            if (jugador!=null && minutoJuego!=0 && arco!="")
            {
                if (jugador.Equipo == Equipo1)
                {
                    Gol nuevoGolEquipo1 = new Gol(jugador, enContra, minutoJuego, arco);
                    GolesEquipo1.Add(nuevoGolEquipo1);
                    return true;
                }
                else
                {
                    Gol nuevoGolEquipo2 = new Gol(jugador, enContra, minutoJuego, arco);
                    GolesEquipo2.Add(nuevoGolEquipo2);
                    return true; 
                }
            }
            return false;
        }
        public bool CargarIncidencia(Jugador jugador, int minutoJuego, Tarjeta.Colores color, string tarjetaAsociada)
        {
            if (jugador !=null && minutoJuego!=0)
            {
                Tarjeta nuevaTarjeta = new Tarjeta(jugador, minutoJuego, color , tarjetaAsociada);
                if (tarjetaAsociada != null)
                {
                    TarjetasAmarillas.Add(nuevaTarjeta);
                    color = Tarjeta.Colores.Roja;
                    tarjetaAsociada = null;
                    nuevaTarjeta = new Tarjeta(jugador, minutoJuego, color, tarjetaAsociada);
                    TarjetasRojas.Add(nuevaTarjeta);
                }
                else
                {
                    if (color == Tarjeta.Colores.Roja)
                    {
                        TarjetasRojas.Add(nuevaTarjeta);
                    }
                    else
                    {
                        TarjetasAmarillas.Add(nuevaTarjeta);
                    }
                }
                return true;
            }
            return false;
        }

        public bool CragarIncidencia(Jugador jugadorEntra, Jugador jugadorSale, int minutoJuego)
        {
            if (jugadorEntra!=null && jugadorSale!=null && minutoJuego!=0)
            {
                Cambio nuevoCambio = new Cambio();
                nuevoCambio.JugadorEntra = jugadorEntra;
                nuevoCambio.JugadorSale = jugadorSale;
                nuevoCambio.MinutoJuego = minutoJuego;
                Cambios.Add(nuevoCambio);
                return true;
            }
            return false;
        }


        ////////////////////////////////////////////////// 10 ////////////////////////////////////////////////////////
       
        public List<string> ObtenerDescripciónTotal()
        {
            List<string> listaIncidencias = new List<string>();
            List<Incidencia> listaIncidenciasOriginal = new List<Incidencia>();

            listaIncidenciasOriginal.AddRange(GolesEquipo1);
            listaIncidenciasOriginal.AddRange(GolesEquipo2);
            listaIncidenciasOriginal.AddRange(Cambios);
            listaIncidenciasOriginal.AddRange(TarjetasAmarillas);
            listaIncidenciasOriginal.AddRange(TarjetasRojas);
            listaIncidenciasOriginal = listaIncidenciasOriginal.OrderBy(x => x.MinutoJuego).ToList();

            foreach (var incidencia in listaIncidenciasOriginal)
            {
                listaIncidencias.Add(incidencia.ObtenerDescripcion());
            }
            return listaIncidencias;
        }

        /////////////////////////////////////////////////////  11   ///////////////////////////////////////////////////////
        public string ObtenerPuntajeFinal()
        {
            int Puntaje1 = 0;
            int Puntaje2 = 0;
            foreach (var gol in GolesEquipo1)
            {
                if (gol.EnContra)
                {
                    Puntaje2 ++;
                }
                else
                {
                    Puntaje1++;
                }
            }
            foreach (var gol in GolesEquipo2)
            {
                if (gol.EnContra)
                {
                    Puntaje1++;
                }
                else
                {
                    Puntaje2++;
                }
            }
            return $"El resultado final es de {Puntaje1} para {Equipo1} y de {Puntaje2} para {Equipo2}";
        }
    }
}
