﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Arbitro:Persona
    {
        public Tipos Tipo { get; set; }

        public enum Tipos
        {
            Principal=1,
            Linea=2,
            Cuarto=3,
        }
        public override bool ProximoRetiro()
        {
            return base.ProximoRetiro();
        }
    }
}
